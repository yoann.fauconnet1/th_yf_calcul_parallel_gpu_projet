MetaData for each scripts of each subparts of the project and results files.
Each results was produced by running atacseq_wf.sh
source code of each scripts is available on our gitlab : https://gitlab.com/yoann.fauconnet1/th_yf_calcul_parallel_gpu_projet/-/tree/main

QUALITY CONTROL : 

Authors : Goué Nadia
Origin : Nadia Goué shared scripts for the project, copy from the server to the server several times between October 25 and November 18 on 2022
Scripts :
- scripts/atac_qc_init.slurm : Initial quality control
- scripts/atac_trim.slurm : Trimming of each 12 samples
- scripts/atac_qc_post.slurm : Post trimming quality control
Modifications :	Input and Output pathway
Results : 
- results/qc/*/fastqc_init/*_fastqc.html : HTML files corresponding to the initial QC for the 6 conditions
- results/qc/*/fastqc_init/*fastqc.html : HTML files corresponding to the post trimming QC for the 6 conditions
- results/trim/*/*.fastq.gz files corresponding to trimmed samples files

MAPPING :

Authors : Goué Nadia
Origin : Nadia Goué shared scripts for the project, copy from the server to the server several times between October 25 and November 18 on 2022
Scripts :
- atac_bowtie2.slurm : 6 paired and already trimmed samples aligned on Mus musculus GRCm39 reference genome
Version : Bowtie2/2.3.4.3, gcc/4.8.4, samtools/1.3
Modifications : Input and Output pathway
Results :
- results/bowtie2/*/*.bam : 6 BAM files corresponding to the alignments of our 6 conditions

DEEPTOOLS :
 
Deeptools manual : https://deeptools.readthedocs.io/en/develop/
Authors : Hamadouche Toufik
	  Fauconnet Yoann
Inputs : In this section, we works with BAM files produced by Bowtie2. 
Scripts : 
- DeepT_MltBamSumry.slurm
- plotCoverage.slurm
- plot_corr_script.slurm
- 
Results : 
- results/deeptools/*.png : 3 files (From plotCoverage) with coverage plot of R0 (3 samples), R24 (3 samples) and both (6 samples)
- results/deeptools/heatmap*.pdf : 2 heatmaps from plot_coor_script.slurm, one with Pearson correlation statistique and the other one with spearman. 

MACS2 : 

MACS manual : https://pypi.org/project/MACS2 
Authors : Hamadouche Toufik
          Fauconnet Yoann
Scripts : 
- script_macs2.slurm
- script_Select_significatif_Peaks.r
- script_Tolanche_Rscript.slurm
Results :
- results/macs2/*.r : an r file for further analysis
- results/macs2/*.bed : For each BAM file from Bowtie, a bed with the position of each peak on the genome
- results/macs2/*.xls : For each BAM file from Bowtie, a table with importants information about peaks : Start and end pos for example
- results/macs2/*.bed : Files with only significatives peaks from the initial BED (noise removing)
- results/macs2/*.pdf : Graphical representation of peaks and their value on the reference genome

BEDTOOLS :

Bedtools manual : https://bedtools.readthedocs.io/en/latest/
Authors : Hamadouche Toufik
Scripts :
- Bed_intersect.slurm : between 0h and each 24h --> 6 resulting bed (bed with overlaps and bed without)
- Bed_multiinter.slurm : between 0h and each 24h --> 3 resulting bed (bed with overlaps)
Results : 
- results/bedtools/*bed : 2 types of files, we can observe atac-seq reads with no overlapping between tested conditions or reads with overlap. Multiinter add a count.
