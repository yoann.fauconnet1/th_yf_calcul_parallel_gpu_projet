#!/bin/bash
#SBATCH --time=0:50:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=2G
#SBATCH --cpus-per-task=8   # Nb of threads we want to run on
#SBATCH -o log/slurmjob-%A-%a
#SBATCH --job-name=Bed_Intersect
#SBATCH --partition=short
#SBATCH --array=0-11

# Program configuration
__author__='Toufik Hamadouche'
__email__='toufik.hamadouche@etu.uca.fr'
__credits__=["Toufik_Hamadouche"]
__license__='GPL3'
__maintainer__='Toufik-H'
__status__='Modéfication of nadia script'
__version__='0.0.1'

echo 'Run Bed_Intersect script on filtred bed.files'

# Handling errors
set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
set -o verbose
set -euo pipefail

IFS=$'\n\t'

# Upload whatever required packages
module purge
module load gcc/4.8.4 bedtools/2.27.1

echo "Set up Data directorie ..." >&2
# Set up for data  directories
DATA_DIR="$HOME"/atacseq/results/MACS_2/Filtred_Peaks/Cleaned_Peaks_


echo "Set up an array with h0-* bed files..." >&2                   
tab=($(ls "$DATA_DIR"/50k_0h_R*_summits.bed))
echo "tab = " >&2
printf '%s\n' "${tab[@]}" >&2


# Set up the temporary directories
shortname=($(basename "${tab[$SLURM_ARRAY_TASK_ID]}" .bed))


# Make shortname for output files of BedTools :  the output file will take shortname of h0 samples
shortname1=$(echo $shortname | sed 's/Macs2_summits//g'  )


# Set up  Scratchdir 
SCRATCHDIR=/storage/scratch/"$USER"/"$shortname1"/Bed_Intersect-Results 

# Set up results directories
OUTPUT="$HOME"/atacseq/results/BedTools_results


# make directorie to SCRATCHDIR & OUTPUTDIR
mkdir -p "$OUTPUT"
mkdir -p -m 700 "$SCRATCHDIR"

cd "$SCRATCHDIR"
mkdir -p "$SCRATCHDIR"


# Run the program:
echo "Run the Intersect  on bed files " >&2
# run script Intersect tool on all filtred  bed files :

##### -1

# Here : intersection h0-R1 VS all h24 (intersect entre chaque h0 avec les h24 resultats --> dans le meme fichier de sortie)
intersectBed -ubam  -a "${tab[$SLURM_ARRAY_TASK_ID]}" -b "$DATA_DIR"/50k_24h_R*-`date +%d-%m-%Y`-Macs2_summits.bed  >Intersect_"$shortname1"VS-h24.bed



#Here : intersection h0-R1 VS all h24 --> NO intersection
intersectBed -ubam  -v -a "${tab[$SLURM_ARRAY_TASK_ID]}" -b "$DATA_DIR"/50k_24h_R*-`date +%d-%m-%Y`-Macs2_summits.bed >No-Intersect_"$shortname1"VS-h24.bed


# + + + + + + + + + + + + + + + + +

##### -2

## compraison inter condition --> puis intersection entre output intersect_All_h0.bed et intersect_All_h24.bed --> scriptIntersect2

# Get Peaks intersection between All H0 conditions samples :
intersectBed -ubam  -a $DATA_DIR/50k_{0..0}h_R{1..1}-`date +%d-%m-%Y`-Macs2_summits.bed -b "$DATA_DIR"/50k_{0..0}h_R{2..3}-`date +%d-%m-%Y`-Macs2_summits.bed  >inter-intersect_All_h0.bed



# Get Peaks intersection between All H24 conditions samples :
intersectBed -ubam  -a $DATA_DIR/50k_{24..24}h_R{1..1}-`date +%d-%m-%Y`-Macs2_summits.bed -b "$DATA_DIR"/50k_{24..24}h_R{2..3}-`date +%d-%m-%Y`-Macs2_summits.bed  >inter-intersect_All_h24.bed


# Details ::
# -v pour les non intersection


cp -r "$SCRATCHDIR" "$OUTPUT"

#Cleaning in case something went wrong
rm -rf "$SCRATCHDIR"


# In the end of scripts
echo "Stop job : "`date` >&2
