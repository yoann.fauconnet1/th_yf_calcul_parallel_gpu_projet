#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} else if (length(args)==2) {
  # default output file
  args[3] = "filtredPeaks.txt"
}

library(stringr)

# this R script is for the Selection of the significant peaks based on the q values 
# By Q values:

# read in the peak data --> samples : 
pdata <- read.table(file=args[1], sep="\t", as.is=T)

pdata=data.frame(pdata)
means = mean(pdata[,c(5)])

#Filtring data by q values columns
pdata=pdata[pdata["V5"] > means,]


#Write new table of significatif Peaks 
write.table(pdata[, c(1,2,3,4,5)], file=args[2], sep="\t",row.names=FALSE, col.names=FALSE)


#Save the barplot in a PDF file, to view the barplot after filtration
pdf(args[3],height=6,width=6)
barplot(pdata[,c(5)],main = "Fold-Enrch _ Pics",xlab = "Pics_name", ylab = "Fold change",names.arg = pdata["V4"],col="#822456",las=2,cex.names=0.8)
dev.off() 
