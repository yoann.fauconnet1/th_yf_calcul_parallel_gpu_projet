#! /bin/bash

#cd "$(dirname "$0")"

echo 'Yoann - Toufik  (Universite Clermont Auvergne, Students)'
echo 'Date: Project '
echo 'Object: Sample case of ATACseq workflow showing job execution and dependency handling.'
echo 'Inputs: paths to scripts qc, trim and bwa'
echo 'Outputs: trimmed fastq files, QC HTML files and BAM files'

# Handling errors
#set -x # debug mode on
set -o errexit # ensure script will stop in case of ignored error
set -o nounset # force variable initialisation
#set -o verbose

IFS=$'\n\t'

# first job - no dependencies :
#############################################################################################################
#### -> we thought about the fact that we are not obliged to inform the same dependencies in 'jid1' for all \
#      the scripts (because some scripts need more dependencies than the others)  
##############################################################################################################


# -------------------------------------- Start : QC - Alignement - cleaning Bam files -------------------------

### 1- Initial QC :
jid1=$(sbatch --parsable /home/users/student04/atacseq/scripts/atacseq_scripts/atac_qc_init.slurm)
echo "$jid1 : Initial Quality Control --> QC-init"


### 2-Trimming
jid2=$(sbatch --parsable --dependency=afterok:$jid1 /home/users/student04/atacseq/scripts/atacseq_scripts/atac_trim.slurm)
echo "$jid2 : Trimming with Trimmomatic tool"


### 3-Second QC
jid3=$(sbatch --parsable --dependency=afterany:$jid2 /home/users/student04/atacseq/scripts/atacseq_scripts/atac_qc_post.slurm)
echo "$jid3 : Second quality control --> QC-Post"


### 4-Bowtie2 alignement
jid4=$(sbatch --parsable --dependency=afterany:$jid3 /home/users/student04/atacseq/scripts/atacseq_scripts/atac_bowtie2.slurm)
echo "$jid4 : Bowtie2 --> aligning Seq reads to long genom reference "


### 5-Picard : Remove PCR duplicates
jid5=$(sbatch --parsable --dependency=afterany:$jid4 /home/users/student04/atacseq/scripts/atacseq_scripts/atac_picard.slurm)
echo "$jid5 : Picard --> remove PCR duplicates"




# ----------------------------------------------  DeepTools ----------------------------------------

### 6- Multibamsummary : 
jid6=$(sbatch --parsable --dependency=afterany:$jid5 /home/users/student04/atacseq/scripts/DeepTools_scripts/MultiBamSumry.slurm)
echo "$jid6 : Multibamsummary Build -->  used in plot corr + Pca + .."


### 7- Plot correaltion between bam files 
jid7=$(sbatch --parsable --dependency=afterany:$jid6 /home/users/student04/atacseq/scripts/DeepTools_scripts/plot_corr_script.slurm)
echo "$jid7 : Plot corr based on Multibamsummary matrix"


### 8- Plot Pca between Bam files 
jid8=$(sbatch --parsable --dependency=afterany:$jid6 /home/users/student04/atacseq/scripts/DeepTools_scripts/plot_Pca_script.slurm)
echo "$jid8 : Plot Pca based on Multibamsummary matrix"





# ----------------------------------------------  MACS (vers 2 )  ----------------------------------------

		# Pour les dépendence, je préfere que MACS2 démmare apres les Deeptools

### 9- MACS(ver:2) - reserche of accessible sites on the chromatin
jid9=$(sbatch --parsable --dependency=afterany:$jid8 /home/users/student04/atacseq/scripts/MACS2_script/script_macs2.slurm)
echo "$jid9 : MACS-2  --> Accesible sites on chromatins"


### 10- Explore output R script of MACS2
jid10=$(sbatch --parsable --dependency=afterany:$jid9 /home/users/student04/atacseq/scripts/MACS2_script/script_reading_R_file.slurm)
echo "$jid10 : Plot what in r script producted by MACS2"


### 11-	Peaks Filtration : significatifs peaks
jid11=$(sbatch --parsable --dependency=afterany:$jid9 /home/users/student04/atacseq/scripts/MACS2_script/script_launche_FilterPeaks.slurm)
echo "$jid11 : Peaks-filtred for Significatif Peaks on MACS2 output"


# ----------------------------------------------  BedTools ---------------------------------------

		#Pour les dépences dans cette étapes : les BedTools devrait attendre le resultats de filtration des Peaks : job11 ici

### 12- Closest : BedTools : Overlaping genomics intervas
jid12=$(sbatch --parsable --dependency=afterany:$jid11 /home/users/student04/atacseq/scripts/BedTools_scripts/Bed_Closest.slurm)
echo "$jid12 : Closest"


### 13- Intersect 1 : BedTools : if the features in the two sets “overlap” with one another.                    
jid13=$(sbatch --parsable --dependency=afterany:$jid11 /home/users/student04/atacseq/scripts/BedTools_scripts/Bed_intersect.slurm)
echo "$jid13 : Intersection Peaks"


### 14- Intersect 2 : BedTools : if the features in the two sets “overlap” with one another. --> use the output of intersect 1 = job13 ici
jid14=$(sbatch --parsable --dependency=afterany:$jid13 /home/users/student04/atacseq/scripts/BedTools_scripts/Bed_intersect2.slurm)
echo "$jid14 : Intersection2 Peaks"


### 15- Multinter : BedTools : identifies common intervals among multiple sorted BED files.
jid15=$(sbatch --parsable --dependency=afterany:$jid14 /home/users/student04/atacseq/scripts/BedTools_scripts/Bed_multiinter.slurm)
echo "$jid15 : Multiinter  "



# ----------------------------------------------  End : Cat dependences ----------------------------

# show dependencies in squeue output:
squeue -u $USER -o "%.8A %.4C %.10m %.20E"
